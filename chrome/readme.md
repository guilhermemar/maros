# Chrome

Para aproveitar melhor alguns sites, é preciso mudar o user-agent. Para isso usamos o plugin __User-Agent Switcher for Chrome__

## Itaú
Utilizar sem precisar instalar o programa de segurança.

__url__  
internetpf.itau.com.br, internetpf1.itau.com.br  
__agent__  
Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36

## Instagram
Para o site liberar a opção de upload de imagem.

__url__  
instagram.com  
__agent__  
Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36
