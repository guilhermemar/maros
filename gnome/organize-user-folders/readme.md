# Gnome default folrders
You can change the default names or/and locations for default user folders on system.

First open the file `~/.config/user-dirs.dirs` to edit.
The content of the file should look like something this

```
XDG_DESKTOP_DIR="$HOME/Desktop"
XDG_DOCUMENTS_DIR="$HOME/Documents"
XDG_DOWNLOAD_DIR="$HOME/Downloads"
XDG_MUSIC_DIR="$HOME/Music"
XDG_PICTURES_DIR="$HOME/Pictures"
XDG_PUBLICSHARE_DIR="$HOME/Public"
XDG_TEMPLATES_DIR="$HOME/Templates"
XDG_VIDEOS_DIR="$HOME/Videos"
```

You can change places, but you can not remove them. If you do not want a folder, you can change the location to a hidden folder, something like this:

```
XDG_DOWNLOAD_DIR="$HOME/Downloads"
XDG_DOCUMENTS_DIR="$HOME/Documents"
XDG_PICTURES_DIR="$HOME/Pictures"
XDG_DESKTOP_DIR="$HOME/.hidden/Desktop"
XDG_TEMPLATES_DIR="$HOME/.hidden/Templates"
XDG_PUBLICSHARE_DIR="$HOME/.hidden/Public"
XDG_MUSIC_DIR="$HOME/.hidden/Music"
XDG_VIDEOS_DIR="$HOME/.hidden/Videos"
```

the unnecessary folder were placed into the hidden folder `.hidden`.
