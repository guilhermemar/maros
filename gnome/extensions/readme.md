# Gnome extensions

removendo os que vem com o ubuntu
```bash
sudo apt remove gnome-shell-extension-ubuntu-dock gnome-shell-extension-desktop-icons
```

Instalar conector:
```bash
sudo apt install chrome-gnome-shell
```

## Basics


* https://extensions.gnome.org/extension/750/openweather/
* https://extensions.gnome.org/extension/906/sound-output-device-chooser/
* https://extensions.gnome.org/extension/517/caffeine/
* https://extensions.gnome.org/extension/1276/night-light-slider/
* https://extensions.gnome.org/extension/7/removable-drive-menu/
* https://extensions.gnome.org/extension/1112/screenshot-tool/
* https://extensions.gnome.org/extension/1267/no-title-bar/
* https://extensions.gnome.org/extension/1011/dynamic-panel-transparency/
* https://extensions.gnome.org/extension/36/lock-keys/
* https://extensions.gnome.org/extension/15/alternatetab/
* https://extensions.gnome.org/extension/19/user-themes/

##  Others

* https://extensions.gnome.org/extension/1031/topicons/
* https://extensions.gnome.org/extension/55/media-player-indicator/
* https://extensions.gnome.org/extension/1383/lyrics-finder/
* https://extensions.gnome.org/extension/792/shutdowntimer/
* https://extensions.gnome.org/extension/413/dash-hotkeys/
* https://extensions.gnome.org/extension/983/duolingo-status/
* https://extensions.gnome.org/extension/690/easyscreencast/
* https://extensions.gnome.org/extension/1125/github-notifications/
* https://extensions.gnome.org/extension/836/internet-radio/
* https://extensions.gnome.org/extension/1040/random-wallpaper/
* https://extensions.gnome.org/extension/355/status-area-horizontal-spacing/
* https://extensions.gnome.org/extension/1080/transparent-notification/
* https://extensions.gnome.org/extension/1227/window-corner-preview/
* https://extensions.gnome.org/extension/53/pomodoro/
* https://extensions.gnome.org/extension/1403/remove-alttab-delay/
* https://extensions.gnome.org/extension/800/remove-dropdown-arrows/


## Removing Ubuntu extensions

```bash
$ sudo apt remove gnome-shell-extension-ubuntu-dock
```

## Adding vanilla gnome settings:

```bash
$ sudo apt install vanilla-gnome-default-setting
```
