# Atom

```bash
$ sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
$ curl -sL https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
$ sudo apt-get update
```

```bash
# Install Atom
$ sudo apt-get install atom
# Install Atom Beta
$ sudo apt-get install atom-beta
```

## Extensões
* editorconfig
* language-vue
* language-docker
* language-xml
* language-markdown
* linter-eslint
* linter-sass-lint
* firacode
* logo-file-icons

### Discontinued
* atom-ide-ui
* ide-typescript
* ide-json
* ide-vue
* ide-css
* ide-html
* ide-bash

__note:__ Use `atom-ide-diagnostics`, then don't need install `linter` package.


## Configurações

### autocomplete-plus
Desabilitar a opção "_show Suggestions On Keystroke_"
